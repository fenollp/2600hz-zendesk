#!/usr/bin/env python3

from creds import creds
from zenpy import Zenpy
import datetime
import re

zenpy = Zenpy(email=creds['u'], password=creds['p'], subdomain='2600hz')

def latest_unsolved_tickets():
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    today = datetime.datetime.now()
    # rep = zenpy.search(query='status<solved' , type='ticket', sort_by='created_at', sort_order='desc')
    rep = zenpy.search(query='status:open' , type='ticket', sort_by='created_at', sort_order='desc')
    print('Latest unsolved tickets:', rep.count)

    for ticket in rep:
        reqids(ticket.id)

def created_at(comment):
    return comment.get('created_at')

def attachments(comment):
    return comment.get('attachments')

def body(comment):
    return comment.get('plain_body')

def find(regexp, txt):
    return list(set(regexp.findall(txt)))

reqid_reg = re.compile('req.*?id.*?([0-9a-f]{32})', flags=re.IGNORECASE)
maybe_reqid_reg = re.compile('([0-9a-f]{15,})', flags=re.IGNORECASE)
def reqids(ticket_id):
    print('https://2600hz.zendesk.com/agent/tickets/' + str(ticket_id))
    comments = zenpy.tickets.comments(ticket_id=ticket_id).values
    print('  ', len(comments), 'comments:')
    co_first_txt = body(comments[0])
    for nth, com in enumerate(comments):
        print('  ', str(nth+1)+'th:', 'attachments:', len(attachments(com)), 'creation:', created_at(com))
        reqids = find(reqid_reg, body(com))
        if len(reqids) > 0:
            print('  ', '  ', 'req ids:', len(reqids))
            for reqid in reqids:
                print('  ', '  ', '  ', reqid)
        maybe_reqids = find(maybe_reqid_reg, body(com))
        maybe_reqids = [x for x in maybe_reqids if not x in reqids]
        if len(maybe_reqids) > 0:
            print('  ', '  ', 'potential req ids:', len(maybe_reqids))
            for reqid in maybe_reqids:
                print('  ', '  ', '  ', reqid)



import sys
reqids(sys.argv[1])

# latest_unsolved_tickets()
