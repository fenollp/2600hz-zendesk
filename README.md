# https://bitbucket.org/fenollp/2600hz-zendesk

Find request ids in latest unsolved tickets

## Requires

* `python3`
* `pip3 install zenpy` (known to work with 1.1.7)

## Adding your Zendesk credentials

Create a file named `creds.py` with:

```python
creds = {
    'u': 'yourHandle@2600hz.com',
    'p': 'yourPassw0rd_ici',
}
```

## Run

`./bot.py`

Lists latest unsolved tickets with creation date & number of comments.
It also prints request ids it finds in comments.
